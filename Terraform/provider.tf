terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.26"
    }
  }
}
provider "azurerm" {
  features {
  }

  subscription_id = "3164e3a1-4196-4ac3-9924-91ce42a14951"
  client_id       = "bf3f29a3-ab27-4933-9f01-dc9187ad8775"
  tenant_id       = "f01e930a-b52e-42b1-b70f-a8882b5d043b"
  client_secret   = var.client_secret
}
