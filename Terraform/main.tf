#Create new resource group
resource "azurerm_resource_group" "rg" {
  name     = "azlbtlab1Final"
  location = "southeastasia"
}

#Create a virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "lab1-vm0-vnet0"
  address_space       = ["10.0.0.0/16"]
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name
}

#Create a subnet0 for linux vm0
resource "azurerm_subnet" "subnet" {
  name                 = "subnet0"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

#publicIP address for vm0
resource "azurerm_public_ip" "publicIP" {
  name                = "vm0_pip0"
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  domain_name_label   = "lab1-vm0final1"
}

#Create subnet1 for linux vm1
resource "azurerm_subnet" "subnet1" {
  name                 = "subnet1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}


#publicIP address for vm1
resource "azurerm_public_ip" "publicIPvm1" {
  name                = "vm1_pip1"
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  domain_name_label   = "lab1-vm1final1"
}

#Create a new NSG
resource "azurerm_network_security_group" "newNSG" {
  name                = "vm0nsg0"
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name
  security_rule {
    access                     = "Allow"
    description                = "SSH inbound rule"
    destination_address_prefix = "*"
    destination_port_range     = "22"
    direction                  = "Inbound"
    name                       = "SSH"
    priority                   = 1001
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }

  /*security_rule {
    access                     = "Allow"
    description                = "Allow port"
    destination_address_prefix = "*"
    destination_port_range     = "5985"
    direction                  = "Inbound"
    name                       = "winrm"
    priority                   = 300
    protocol                   = "Tcp"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }*/

}

#vm-nic0 for linux vm0
resource "azurerm_network_interface" "vm-nic0" {
  name                = "vm0-nic0"
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "nicConfiguration"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicIP.id
  }
}

#vm-nic1 for windows vm1
resource "azurerm_network_interface" "vm-nic1" {
  name                = "vm1-nic1"
  location            = "southeastasia"
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "nic1config"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicIPvm1.id
  }
}

#Create a new Linux VM
resource "azurerm_virtual_machine" "vm0" {
  name                  = "lab1-vm0"
  location              = "southeastasia"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.vm-nic0.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "myOsDisk0"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_lRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "lab1-vm0"
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

}

data "template_file" "auto_logon" {
  template = file("tpl.auto_logon.xml")

  vars = {
    admin_username = var.admin_username
    admin_password = var.admin_password
  }
}

#Create Window VM 
resource "azurerm_virtual_machine" "vm1" {
  name                  = "lab1-vm1"
  location              = "southeastasia"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.vm-nic1.id]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "OsDisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }

  os_profile {
    computer_name  = "lab1-vm1"
    admin_username = var.admin_username
    admin_password = var.admin_password
    custom_data    = file("../source/ConfigureRemotingForAnsible.ps1")
  }

  os_profile_windows_config {
    provision_vm_agent = true
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = data.template_file.auto_logon.rendered
    }

    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = file("tpl.first_logon_commands.xml")
    }
  }
}


/*resource "azurerm_storage_account" "example" {
  name                     = var.storage_name
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = "southeastasia"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "example" {
  name                  = "content"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "example" {
  name                   = "ConfigureRemotingForAnsible.ps1"
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example.name
  type                   = "Block"
  source                 = "ConfigureRemotingForAnsible.ps1"
}

#Run scripts
esource "azurerm_virtual_machine_extension" "ansibleConfig" {
  name                 = "ansibleConfig"
  virtual_machine_id   = azurerm_virtual_machine.vm1.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = <<SETTINGS
  {
    "commandToExecute": "c:/AzureData/CustomData.bin c:/AzureData/CustomData.ps1 && PowerShell -File CustomData.ps1"
  }
  SETTINGS
}*/


#Get vm0 public IP
data "azurerm_public_ip" "publicIP" {
  name                = azurerm_public_ip.publicIP.name
  resource_group_name = azurerm_virtual_machine.vm0.resource_group_name
  depends_on          = [azurerm_virtual_machine.vm0]
}

#Get vm1 public IP
data "azurerm_public_ip" "publicIP1" {
  name                = azurerm_public_ip.publicIPvm1.name
  resource_group_name = azurerm_virtual_machine.vm0.resource_group_name
  depends_on          = [azurerm_virtual_machine.vm1]
}

resource "null_resource" "clear_hosts_file" {
  provisioner "local-exec" {
    command = "echo '' > ../Ansible/hosts"
  }

}
# Write data for ansible hosts (vm0-linux)
resource "null_resource" "export_public_ip_vm0" {
  provisioner "local-exec" {
    command = "echo '[linux_server]\n${data.azurerm_public_ip.publicIP.ip_address}\n\n[linux_server:vars]\nansible_ssh_user=${var.admin_username}\nansible_ssh_pass=${var.admin_password}\n' >> ../Ansible/hosts"
  }
  depends_on = [azurerm_virtual_machine.vm0]
}

# Write data for ansible hosts (vm1-windows)
resource "null_resource" "export_public_ip_vm1" {
  provisioner "local-exec" {
    command = "echo '[win_server]\n${data.azurerm_public_ip.publicIP1.ip_address}\n\n[win_server:vars]\nansible_user=${var.admin_username}\nansible_password=${var.admin_password}\nansible_connection= winrm\nansible_winrm_transport= basic\nansible_winrm_server_cert_validation=ignore' >> ../Ansible/hosts"
    #command = "echo 'test' >> /Users/root1/DevOps-traning-program/Lab1-Final/Ansible/hosts"
    #command = "echo '[win_server]\n${data.azurerm_public_ip.publicIP1.ip_address}' >> /Users/root1/DevOps-traning-program/Lab1-Final/Ansible/hosts"
  }
  depends_on = [azurerm_virtual_machine.vm1]
}

# Turn off host checking and fork safety
resource "null_resource" "turn_off_checking" {
  provisioner "local-exec" {
    command = "export ANSIBLE_HOST_KEY_CHECKING=False"
  }

  /***For MAC user***/
  /*provisioner "local-exec" {
    command = "export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES"
  }*/

  depends_on = [azurerm_virtual_machine.vm1]
}

resource "null_resource" "execute_ansible" {
  provisioner "local-exec" {
    command = "chmod 755 ./run-ansible.sh"
  }

  provisioner "local-exec" {
    command = "./run-ansible.sh"
  }

  depends_on = [null_resource.export_public_ip_vm1]
}

output "virtual_machine_name" {
  value = [azurerm_virtual_machine.vm0.id, azurerm_virtual_machine.vm1.id]
}
