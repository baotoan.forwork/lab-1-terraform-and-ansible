variable "admin_username" {
  type        = string
  description = "administrator user name for virtual machine"
  default     = "lebaotoan"
}

variable "admin_password" {
  type        = string
  description = "Password must meet Azure complexity requirements"
  default     = "Password1234!"
}

variable "storage_name" {
  type    = string
  default = "azlbtsa1"
}

variable "client_secret" {
  default = "rqFnnuMnTMqr24~58wKM1f56_5G-S~eOPZ"
}
